const schedule = require('node-schedule');
const fs = require('fs');
const YAML = require('yaml');
const request = require('request');
const crypto = require('crypto');
const JsDiff = require('diff');
const JaroWrinker = require('./Jaro-wrinker');
const Telegraf = require('telegraf')

const schedulerHandles = [];
const siteResponses = {};

var settings = null;
var telegramBot = null;

/**
 * Pare settings YAML file and create watchers
 */
function readYaml() {
    const file = fs.readFileSync('./settings.yaml', 'utf8')
    settings = YAML.parse(file);
    
    const sites = Object.keys(settings.sites);
    sites.forEach((site) => {
        createNewWatcher(site, settings.sites[site]);
    });
}

/**
 * Create new url watchers.
 * 
 * @param {String} url Url to watch
 * @param {Object} cron Cron configuration
 */
function createNewWatcher(url, cron) {
    const h = schedule.scheduleJob(cron, function(u) {
        scanSite(u);
    }.bind(null, url));

    if(h === null) {
        throw new Error(`Could not init watcher for ${url}. Check your config.`);
    }

    schedulerHandles.push(h);
    console.log(`Scheduled ${url} with cron '${JSON.stringify(cron)}'. Next scan on ${h.nextInvocation()}`);
}

/**
 * Cancel all handles.
 */
function cancelAllHandles() {
    schedulerHandles.forEach((handle) => {
        handle.cancel();
    });
    schedulerHandles.length = 0;
}

/**
 * Scan a site for changes.
 * 
 * @param {String} url Url of the site to scan
 */
function scanSite(url) {
    console.log(`Scanning ${url}.`);
    request(url, { json: true }, (err, res, body) => {
        if (err) { 
            return console.log(err); 
        }

        // Generate hash
        const hash = crypto.createHash('sha256').update(body).digest('base64');

        // If not scanned yet, create a new object
        if(!(url in siteResponses)) {
            console.log('First scan.');
            siteResponses[url] = {
                previousResponse: body,
                previousHash: hash
            }
            return;
        }

        // Check hashes first since diffing is expensive
        if(siteResponses[url].previousHash === hash) {
            console.log('No changes found.');
            return;
        }

        // Calculate the diff
        const difference = Math.round(100 - (JaroWrinker(siteResponses[url].previousResponse, body) * 100));
        var threshold = 0;
        if('alertAbove' in settings.sites[url]) {
            threshold = settings.sites[url].alertAbove;
        }

        if(difference < threshold) {
            console.log(`Difference found, not above alert threshold: ${difference}% different vs threshold of ${threshold}%`);
            return;
        }

        const differenceInChunks = JsDiff.diffLines(siteResponses[url].previousResponse, body);
        var msg = `Change found for ${url}. Site changed ${difference}%. ${differenceInChunks.length - 1} chunks changed.`;
        console.log(msg);
        sendMessageToContacts(msg);
    });
}

/**
 * Setup the Telegram Bot
 */
function setupTelegramBot() {
    telegramBot = new Telegraf(settings['telegram_bot_token']);
    telegramBot.start((ctx) => {
        ctx.reply('Welcome! This is a private bot.');
    });
    telegramBot.launch();

    setTimeout(() => {
        sendMessageToContacts("UrlWatcher started.");
    }, 200);
}

/**
 * Sends a message to all configured contacts.
 * 
 * @param {String} message Message to send
 */
function sendMessageToContacts(message) {
    settings.telegram_users.forEach((id) => {
        telegramBot.telegram.sendMessage(id, message);
    })
}

/**
 * Main function
 */
function main() {
    fs.watchFile('settings.yaml', function () {
        cancelAllHandles();
        readYaml();
    });
    
    process.on('SIGINT', function () {
        fs.unwatchFile('settings.yaml');
        cancelAllHandles();
        process.exit();
    });

    readYaml();
    setupTelegramBot();
}

main();