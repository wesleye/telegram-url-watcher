FROM node

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
COPY package*.json ./
RUN npm ci --only=production

# Add nano so we can update the config while the image is running
RUN apt-get update && apt-get install -y vim nano && rm -rf /var/lib/apt/lists/*

# Bundle app source
COPY . .

CMD [ "node", "main.js" ]