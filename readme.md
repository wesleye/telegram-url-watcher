# Telegram URL Watcher

Notifies you of a changed URL through Telegram. Find your ID by talking to [UserInfoBot](https://telegram.me/userinfobot). You can set the threshold for alerting. The settings will auto reload when changed.

## Get started
```sh
cp setttings.yaml.example settings.yaml
# Add your sites, set your token
node ./main.js
```

## Possible values for sites
```
second (0-59)
minute (0-59)
hour (0-23)
date (1-31)
month (0-11)
year
dayOfWeek (0-6) Starting with Sunday
```
See documentation of [node-schedule](https://www.npmjs.com/package/node-schedule):

> **Note:** It's worth noting that the default value of a component of a recurrence rule is null (except for second, which is 0 for familiarity with cron).

## Examples

Scan every 1 second, alert on any change.
```yml
sites:
  "https://www.google.com":
    second: 1
    alertAbove: 0
```

Scan every hour on sundays, alert when more than 20% of the HTML changed.
```yml
sites:
  "https://www.google.com":
    hour: 0
    dayOfWeek: 6
    alertAbove: 20
```

Scan every minute, alert on any change.
```yml
sites:
  "https://www.google.com":
    second: 0
    alertAbove: 0
```

## Special thanks to
[Suman Kunwar](https://medium.com/@sumn2u/string-similarity-comparision-in-js-with-examples-4bae35f13968)